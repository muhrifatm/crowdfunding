<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'id' => 'aa7b1372-086b-4f96-be0c-b1feb5e4be5e',
                'role_type' => 'admin'
            ],

            [
                'id' => 'd8c63f24-c336-4df2-a237-70377ad2e224',    
                'role_type' => 'user'
                ]
        ]);
    }
}
