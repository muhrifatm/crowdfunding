<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The "booting" function of model
     * 
     * @return void
     */
    protected static function boot(){
        parent::boot();
        static::creating(function($model){
            if ( ! $model->getKey()){
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }
    /**
     * Get the value indicating the IDs are incrementing.
     * 
     * @return bool
     */
    public function getIncrementing(){
        return false;
    }
    /**
     * Get the auto-incrementing key type
     * 
     * @return string
     * 
     */

     public function getKeyType(){
         return 'string';
     }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function otp_codes(){
        return $this->hasOne('App\Otp_codes');
      }

      public function roles()
      {
       return $this->hasMany('App\Roles');
      } 

      public function emailValid(){
        if ($this->email_verified_at != null) {
            return true;
        }
        return false;
    }

    public function isAdmin(){
        if ($this->role_id == 'aa7b1372-086b-4f96-be0c-b1feb5e4be5e') {
            return true;
        }
        return false;
    }
}
