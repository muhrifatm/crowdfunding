<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp_codes extends Model
{
    //

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

     /**
     * The "booting" function of model
     * 
     * @return void
     */
    
    protected static function boot(){
        static::creating(function($model){
            if ( ! $model->getKey()){
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }
    /**
     * Get the value indicating the IDs are incrementing.
     * 
     * @return bool
     */
    public function getIncrementing(){
        return false;
    }
    /**
     * Get the auto-incrementing key type
     * 
     * @return string
     * 
     */

     public function getKeyType(){
         return 'string';
     }
}
